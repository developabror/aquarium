import lombok.SneakyThrows;

import java.util.Objects;
import java.util.Random;
import java.util.Vector;

public class Aquarium {


    int m;
    int f;
    Random random = Store.random;

    public void startProcess() {
        m = random.nextInt(5, 15);
        f = random.nextInt(5, 15);
        for (int i = 0; i < m; i++) {
            Fish fish = new Fish(1, random.nextInt(1, Fish.MAX_AGE), true);
            fish.start();
            Store.fishes.add(fish);
        }
        for (int i = 0; i < f; i++) {
            Fish fish = new Fish(1, random.nextInt(1, Fish.MAX_AGE), false);
            fish.start();
            Store.fishes.add(fish);
        }
        live();
    }

    @SneakyThrows
    private void live() {
        while (true) {
            Thread.sleep(random.nextInt(5000, 15000));
            for (int i = 0; i < 3; i++)
                meet();
            info();
        }
    }

    private void info() {
        System.out.println("\n\nthere are " + Store.fishes.size() + " fishes in the aquarium\n" +
                m + " males\n" + f + " females");
    }

    private void meet() {
        int n1 = random.nextInt(0, Store.fishes.size() - 1);
        int n2 = random.nextInt(0, Store.fishes.size() - 1);
        if (n1 != n2) {
            Fish fish0 = Store.fishes.get(n1);
            Fish fish1 = Store.fishes.get(n2);
            if (fish0.isGender() != fish1.isGender()) {
                int amount = random.nextInt(2, 5);
                System.out.println(fish0.getFishId() + " and " + fish1.getFishId() + " has a meeting and new fishes going to born");
                for (int i = 0; i < amount; i++) {
                    Fish fish = new Fish(1, random.nextInt(1, Fish.MAX_AGE), random.nextBoolean());
                    fish.start();
                    Store.fishes.add(fish);
                    if (fish.isGender())
                        m++;
                    else
                        f++;

                    System.out.println("new " + (fish.isGender() ? "male" : "female") + " fish born");
                }
            }
        }
    }


    private static Aquarium aquarium;

    public static Aquarium getInstance() {
        if (Objects.isNull(aquarium)) {
            aquarium = new Aquarium();
        }
        return aquarium;
    }
}
