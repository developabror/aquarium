import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;

import java.util.UUID;

@Data

public class Fish extends Thread {
    Aquarium aquarium=Aquarium.getInstance();

    public static final int MAX_AGE = 40;
    private UUID fishId = UUID.randomUUID();
    private Integer age;
    private Integer expectedMaxAge;
    private boolean gender;

    public Fish(Integer age, Integer expectedMaxAge, Boolean gender) {
        this.age = age;
        this.expectedMaxAge = expectedMaxAge;
        this.gender = gender;
    }

    @SneakyThrows
    public void run() {

        while (age<=MAX_AGE) {
            Thread.sleep(Store.random.nextInt(5000, 15000));
            addAge();
        }
        die();
    }

    private void addAge() {
        this.age++;
        if (this.age >= expectedMaxAge) {
            die();
        }
    }

    private void die() {
        if (gender)
            aquarium.m--;
        else
            aquarium.f--;
        System.out.println("Fish " + fishId + " died");
        Store.fishes.removeIf(fish -> fish.getFishId().equals(fishId));
        this.stop();
    }

}
